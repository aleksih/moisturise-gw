import logging

from influxdb import InfluxDBClient

logger = logging.getLogger(__name__)


class Writer:
    DATABASE_NAME = 'moisturise'

    @staticmethod
    def data_point_from_measurement(device_name, measurement):
        point = {'measurement': 'soil',
                 'tags': {'protocolVersion': measurement.data_format, 'mac': measurement.mac, 'name': device_name},
                 'time': measurement.timestamp,
                 'fields': {
                     'rssi': measurement.RSSI, 'soilMoisture': measurement.soil_moisture,
                     'temperature': measurement.temperature, 'measurementSequenceNumber': measurement.sequence_number,
                     'identification': measurement.identification, 'calibrationAir': measurement.calibration_air,
                     'calibrationWater': measurement.calibration_water}}
        return point

    def __init__(self, host, port, stop_event, receive_queue):
        self._host = host
        self._port = port
        self._stop_event = stop_event
        self._receive_queue = receive_queue

    def run(self):
        logger.info('Writer started.')
        client = InfluxDBClient(host=self._host, port=self._port)
        client.switch_database(Writer.DATABASE_NAME)

        while not self._stop_event.is_set():
            (device_name, measurement) = self._receive_queue.get()
            logger.debug('Write new data, device: %s, measurements: %s.', device_name, measurement)
            point = Writer.data_point_from_measurement(device_name, measurement)
            if not client.write_points([point]):
                logger.warning('Failed to write a data point (%s) into the database.', str(point))
            self._receive_queue.task_done()
        logger.info('Writer exited.')
