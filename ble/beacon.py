class Beacon:

    def __init__(self, address, RSSI, manufacturer_id, manufacturer_data):
        self._address = address
        self._RSSI = RSSI
        self._manufacturer_id = manufacturer_id
        self._manufacturer_data = manufacturer_data

    @property
    def address(self):
        return self._address

    @property
    def RSSI(self):
        return self._RSSI

    @property
    def manufacturer_id(self):
        return self._manufacturer_id

    @property
    def manufacturer_data(self):
        return self._manufacturer_data

    def __str__(self):
        return '{0}: RSSI: {1}, manufacturer ID: {2}, manufacturer data: {3}'.format(self._address,
                                                                                     self._RSSI,
                                                                                     self._manufacturer_id,
                                                                                     self._manufacturer_data)
