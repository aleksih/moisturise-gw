import logging

import dbus
from bluezero import adapter

from ble import beacon

logger = logging.getLogger(__name__)


class BLEScanner:
    remove_list = set()
    on_device_discovered = None

    @classmethod
    def stop_scan(cls):
        cls.dongle.stop_discovery()

    @classmethod
    def clean_beacons(cls):
        """
        Remove beacons from BlueZ's `devices` list so every advert from a beacon is reported
        """
        not_found = set()
        for beacon in cls.remove_list:
            logger.debug('Remove %s.', beacon)
            try:
                cls.dongle.remove_device(beacon)
            except dbus.exceptions.DBusException as dbus_err:
                if dbus_err.get_dbus_name() == 'org.bluez.Error.DoesNotExist':
                    not_found.add(beacon)
                else:
                    raise dbus_err
        for lost in not_found:
            cls.remove_list.remove(lost)

    @classmethod
    def on_device_found(cls, device):
        manufacturer_id = None
        manufacturer_data = None
        if device.manufacturer_data:
            for mfg_id in device.manufacturer_data:
                manufacturer_id = mfg_id
                manufacturer_data = bytes(device.manufacturer_data[mfg_id])
        new_beacon = beacon.Beacon(device.address, device.RSSI, manufacturer_id, manufacturer_data)
        logger.debug('Discovered device %s', new_beacon)
        if cls.on_device_discovered:
            cls.on_device_discovered(new_beacon)
        cls.remove_list.add(str(device.remote_device_path))
        cls.clean_beacons()

    @classmethod
    def start_scan(cls, on_device_discovered=None):
        cls.on_device_discovered = on_device_discovered
        cls.dongle = adapter.Adapter()
        cls.dongle.on_device_found = cls.on_device_found
        cls.dongle.show_duplicates()
        cls.dongle.start_discovery()
