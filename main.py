import argparse
import ast
import logging
import os

from dotenv import load_dotenv

import gateway

load_dotenv()


def main():
    parser = argparse.ArgumentParser(description='Scan Moisturise devices and save measurements into a database.')
    parser.add_argument('-d', '--debug', action='store_const', dest='loglevel', const=logging.DEBUG,
                        default=logging.WARNING, help='Print debug information')
    parser.add_argument('-v', '--verbose', action='store_const', dest='loglevel', const=logging.INFO, help='Be verbose')
    args = parser.parse_args()
    logging.basicConfig(level=args.loglevel)

    app = gateway.Gateway(os.getenv('INFLUXDB_HOST'), os.getenv('INFLUXDB_PORT'))

    device_names = ast.literal_eval(os.getenv('DEVICE_NAMES'))
    app.set_device_names(device_names)

    app.run()


if __name__ == '__main__':
    main()
