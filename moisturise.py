import struct
from datetime import datetime, timezone

from ble import beacon
from util import normalize_mac_address


def is_moisturise_beacon(device: beacon.Beacon):
    if device.manufacturer_id != Measurement.MANUFACTURER_ID:
        return False
    if device.manufacturer_data[Measurement.offsets['data_format']] != Measurement.DATA_FORMAT_VERSION:
        return False
    if len(device.manufacturer_data) != Measurement.BYTES_IN_DATA:
        return False
    return True


class Measurement:
    MANUFACTURER_ID = 0xFFFF
    DATA_FORMAT_VERSION = 1

    MAX_MOISTURE_VALUE = 40000
    BYTES_IN_DATA = 14
    offsets = {'data_format': 0,
               'soil_moisture': 1,
               'temperature': 3,
               'identification': 5,
               'sequence_num': 8,
               'calibration_air': 10,
               'calibration_water': 12
               }

    @staticmethod
    def deserialize_soil_moisture(data):
        index = Measurement.offsets['soil_moisture']
        raw_value = struct.unpack('>H', data[index:index + 2])[0]
        return float(raw_value) / float(Measurement.MAX_MOISTURE_VALUE)

    @staticmethod
    def deserialize_identification(data):
        index = Measurement.offsets['identification']
        raw_value = struct.unpack('B', data[index:index + 1])[0]
        return raw_value != 0

    @staticmethod
    def deserialize_sequence_number(data):
        index = Measurement.offsets['sequence_num']
        return struct.unpack('>H', data[index:index + 2])[0]

    @staticmethod
    def deserialize_calibration_air(data):
        index = Measurement.offsets['calibration_air']
        raw_value = struct.unpack('>H', data[index:index + 2])[0]
        return float(raw_value) / 1000.0

    @staticmethod
    def deserialize_calibration_water(data):
        index = Measurement.offsets['calibration_water']
        raw_value = struct.unpack('>H', data[index:index + 2])[0]
        return float(raw_value) / 1000.0

    def __init__(self, device: beacon.Beacon):
        self.timestamp = datetime.now(timezone.utc).strftime('%Y-%m-%dT%H:%M:%SZ')
        self.mac = normalize_mac_address(device.address)
        self.RSSI = device.RSSI
        self.data_format = device.manufacturer_data[Measurement.offsets['data_format']]
        self.soil_moisture = Measurement.deserialize_soil_moisture(device.manufacturer_data)
        self.temperature = 0.0
        self.identification = Measurement.deserialize_identification(device.manufacturer_data)
        self.sequence_number = Measurement.deserialize_sequence_number(device.manufacturer_data)
        self.calibration_air = Measurement.deserialize_calibration_air(device.manufacturer_data)
        self.calibration_water = Measurement.deserialize_calibration_water(device.manufacturer_data)

    def __str__(self):
        return '{0}: MAC: {1}, RSSI: {2}, sequence number: {3}, soil moisture: {4:.4} %, temperature: {5:.1} °C, identification: {6}'.format(
            self.timestamp,
            self.mac,
            self.RSSI,
            self.sequence_number,
            self.soil_moisture * 100.0,
            self.temperature,
            self.identification)
