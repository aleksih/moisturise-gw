from queue import Queue
import threading

from bluezero import async_tools

import moisturise
from ble import BLEScanner
from db import Writer
from util import normalize_mac_address


class Gateway:

    def __init__(self, db_host, db_port):
        self._device_names = {}
        self._measurement_sequence_map = {}
        self._measurement_queue = Queue()
        self._db_host = db_host
        self._db_port = db_port

    def set_device_names(self, device_names):
        self._device_names = {}
        for mac, name in device_names.items():
            self._device_names[normalize_mac_address(mac)] = name

    def _on_device_discovered(self, device):
        if moisturise.is_moisturise_beacon(device):
            self._on_new_measurement(moisturise.Measurement(device))

    def _on_new_measurement(self, measurement: moisturise.Measurement):
        if measurement.mac in self._measurement_sequence_map:
            last_sequence_number = self._measurement_sequence_map[measurement.mac]
            if measurement.sequence_number == last_sequence_number:
                return

        self._measurement_sequence_map[measurement.mac] = measurement.sequence_number
        name = self._device_names.get(measurement.mac, measurement.mac)
        self._measurement_queue.put((name, measurement))

    def run(self):
        stop_writer = threading.Event()
        db_writer = Writer(self._db_host, self._db_port, stop_writer, self._measurement_queue)
        db_writer_thread = threading.Thread(target=db_writer.run, name='writer')
        db_writer_thread.start()

        BLEScanner.start_scan(on_device_discovered=self._on_device_discovered)

        mainloop = async_tools.EventLoop()
        try:
            mainloop.run()
        except KeyboardInterrupt:
            mainloop.quit()
            BLEScanner.stop_scan()
        self._measurement_queue.join()
        stop_writer.set()
        db_writer_thread.join()
